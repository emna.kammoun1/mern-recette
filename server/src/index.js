 //the server for famwork to create our API
 import express from "express" //=> ajout "type" : "mudule"
 //const express = require ("express")

 //library that allows the rules to the communication between frontend and backend
 import cors from 'cors'

 //arm for mongodb
 // modéliser la structure de données,créer des modèles qui vous permettent d'interagir avec les collections MongoDB 
 import mongoose from 'mongoose'
 
 import {userRouter} from './routes/users.js'
 import {recipesRouter} from './routes/recipes.js'

 //initializing an Express application
 const app=express()

 //ajouter middleware permet de parser requêtes HTTP en JSON
 app.use(express.json())
 // activer le middleware CORS
 app.use(cors())

 app.use("/auth" ,userRouter)
 app.use("/recipes" ,recipesRouter)

 //genrrate a connection
 mongoose.connect("mongodb+srv://emnakammoun:F7pDVFrgrJBFd4oS@recipes.0u5zlf1.mongodb.net/recipes?retryWrites=true&w=majority")

 // ajout   "scripts": {
    //"start": "nodemon src/index.js"}, automatique
 app.listen(3001 , () => console.log("SERVER STARTED"))
 //F7pDVFrgrJBFd4oS
 //emnakammoun