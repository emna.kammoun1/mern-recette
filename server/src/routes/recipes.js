import express from "express" 
import { RecipeModel } from "../models/Recipes.js"
import { UserModel } from "../models/Users.js"
import { verifyToken } from "./users.js";

const router = express.Router()
//récupérer toutes les recettes présentes dans la base de données
router.get("/" , async (req, res) => {
    try {
        const response = await RecipeModel.find({})
        res.json(response)
    } catch (err) {
        res.json(err)
    }
})

// créer une nouvelle recette 
router.post("/",verifyToken, async (req, res) => {
    const recipe = new RecipeModel(req.body)

    try {
        const response = await recipe.save()
        res.json(response)
    } catch (err) {
        res.json(err)
    }
})

//ajouter une recette à la liste des recettes sauvegardées par un utilisateur
router.put("/",verifyToken, async (req, res) => {

    try {
        const recipe = await RecipeModel.findById(req.body.recipeID)
        const user = await UserModel.findById(req.body.userID)
        user.savedRecipes.push(recipe )
        await user.save()
        res.json({savedRecipes : user.savedRecipes})
    } catch (err) {
        res.json(err)
    }
})

// récupérer les identifiants recettes sauvegardées par un utilisateur
router.get ("/savedRecipes/ids/:userID", async(req, res) => {
    try {
        const user = await UserModel.findById(req.params.userID)
        res.json({savedRecipes : user?.savedRecipes})

    }catch (err) {
        res.json(err)
    }
})

// récupérer les recettes sauvegardées par un utilisateur spécifique, identifié par son ID
router.get ("/savedRecipes/:userID", async(req, res) => {
    try {
        const user = await UserModel.findById(req.params.userID)
        const savedRecipes = await RecipeModel.find ( {
            _id : { $in : user.savedRecipes}
        })
        res.json({savedRecipes})

    }catch (err) {
        res.json(err)
    }
})




export { router as recipesRouter}


