import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Cookies, useCookies } from "react-cookie";
import {useGetUserID} from "../hooks/useGetUserID"
export const CreateRecipe = () => {
  const userID = useGetUserID()
  const [cookies, _] = useCookies(["access_token"]);

  const [recipe, setRecipe]= useState({
    name : "" ,
    ingredients: [],
    instructions: "" ,
    imageUrl :"" ,
    cookingTime : 0,
    userOwner: userID,
    
  })

  const navigate =useNavigate()

  //function to 
  const handleChange = (event) => {
    //accecing the name
    // l'utilisateur tape dans un champ de formulaire, event.target sera l'objet représentant ce champ.
    const{name , value} = event.target
    //value contient la valeur actuelle de l'élément
    setRecipe({ ...recipe,[name] : value})
//mettre à jour la propriété spécifique dans l'obje
  }
  const addIngredient =() => {
    setRecipe({...recipe, ingredients: [...recipe.ingredients,""]})
  }

  //gérer les modifications des champs d'un formulaire associés aux ingrédients
  const handleIngredientChange = (event ,idx) => {
    //accecing the name
    // l'utilisateur tape dans un champ de formulaire, event.target sera l'objet représentant ce champ.
   //le champ de formulaire associé à l'ingrédient
    const{value} = event.target
    // récupère la liste des ingrédients actuels à partir de l'objet recipe récupère la liste des ingrédients actuels à partir de l'objet recipe
    const ingredients = recipe.ingredients
    //met à jour la valeur de l'ingrédient de l'indice idx par value
    ingredients[idx] =value
  
    //value contient la valeur actuelle de l'élément
    setRecipe({ ...recipe, ingredients: ingredients });
    //mettre à jour la propriété spécifique dans l'objet
    console.log(recipe)
  }

  const onSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.post(
        "http://localhost:3001/recipes",
        { ...recipe },
        {
          headers: { authorization: cookies.access_token },
        }
      );

      alert("Recipe Created");
      navigate("/");
    } catch (error) {
      console.error(error);
    }
  };
  
    return (
      <div className="create-recipe">
        <h2> Create Recipe</h2>
        <form onSubmit={onSubmit}>
          <label htmlFor="name"> Name : </label>
          <input type="text" id="name"  name="name" onChange={handleChange}/>

          <label htmlFor="ingredients"> Ingredients : </label>
          {recipe.ingredients.map((ingredient , idx) => (
            <input key={idx} type="text" name="ingredients" value={ingredient} onChange={(event) => handleIngredientChange(event ,idx)}></input>
          ))}
          <button onClick={addIngredient} type="button"> Add Ingredient</button>
          <label htmlFor="instructions"> Instructions : </label>
          <textarea id="instructions" name="instructions"onChange={handleChange}></textarea>

          <label htmlFor="imageUrl"> Image Url : </label>
          <input type="text" id="imageUrl" name="imageUrl" onChange={handleChange}/>

          <label htmlFor="cookingTime"> Cooking Time : </label>
          <input type="number" id="cookingTime" name="cookingTime" onChange={handleChange}/>
          <button  type="submit"> Create recipe </button>

        </form>
        
      </div>
    )
  }
  
  